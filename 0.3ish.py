import xgboost as xgb
import pandas as pd
from sklearn.model_selection import train_test_split

from dataProcessing import readData, prepareDataBasic


def main():

    train, test = readData()

    final = test.copy()

    test['consumption'] = 0

    # Pre-process data

    train = prepareDataBasic(train)
    test = prepareDataBasic(test)

    xgb_models = []
    predictions = []

    #Create list of models
    for i in range(7):
        
        _train = train[(train['hour'] == i)]

        model_train, model_test = train_test_split(_train, test_size=0.1)

        d_train = xgb.DMatrix(model_train.drop(columns=['consumption']), model_train['consumption'])
        d_test = xgb.DMatrix(model_test.drop(columns=['consumption']), model_test['consumption'])

        #'tree_method': 'gpu_hist'

        xgb_params = {'nthread': -1, 'learning_rate': 0.0001, 'max_depth': 8, 'min_child_weight': 0.7, 'subsample': 0.7, "eval_metric": "mae"}

        watchlist = [(d_train, 'train'), (d_test, 'val')]

        print(f"Training for day {i}")

            #verbose_eval=False

        xgb_model = xgb.train(xgb_params, d_train, 500000, watchlist, early_stopping_rounds = 50, verbose_eval=1000)

        xgb_models.append(xgb_model)

    #now apply....

    print(f"Training now complete")

    for i, row in test.iterrows():
        
        good_model = xgb_models[int(row.weekday)]

        prediction = good_model.predict(xgb.DMatrix(test.iloc[[i]].drop(columns=['consumption'])))

        predictions.append(prediction)

    final['consumption'] = pd.DataFrame(predictions)

    final = final[['time', 'consumption']]

    final.to_csv('submission.csv', index=False)

    print('Successfull save!')

    return


if __name__ == "__main__":
    main()