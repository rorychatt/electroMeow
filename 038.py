import xgboost as xgb
from sklearn.model_selection import train_test_split

from dataProcessing import readData, prepareDataBasic

def main():

    # Read data
    train, test = readData()

    final = test.copy()

    test['consumption'] = 0

    # Pre-process data

    train = prepareDataBasic(train)
    test = prepareDataBasic(test)

    #feature_names = ['temp', 'dwpt', 'rhum', 'prcp', 'snow', 'wdir', 'wspd', 'wpgt', 'pres', 'el_price', 'day', 'month', 'hour']


    #remove weird parameters, why are they here?


    train.drop(columns=['dwpt', 'rhum', 'snow', 'wdir', 'pres'])
    test.drop(columns=['dwpt', 'rhum', 'snow', 'wdir', 'pres'])


    model_train, model_test = train_test_split(train, test_size=0.25)


    d_train = xgb.DMatrix(model_train.drop(columns=['consumption']), model_train['consumption'])
    d_test = xgb.DMatrix(model_test.drop(columns=['consumption']), model_test['consumption'])

    test = test.drop(columns=['consumption'])

    d_submission = xgb.DMatrix(test)

    #xgb_params = { 'max_depth': 7, 'lambda': 1, 'nthread': -1 }

    # xgb_params = {'min_child_weight': 0.2, 'eta': 0.1, 'colsample_bytree': 0.9, 
    #               'max_depth': 5, 'subsample': 0.9, 'lambda': 1., 
    #               'nthread': -1, 'booster' : 'gbtree', 'silent': 1,
    #               'eval_metric': 'rmse', 'objective': 'reg:linear'}

    xgb_params = {'nthread': -1, 'learning_rate': 0.005, 'max_depth': 12, 'min_child_weight': 1, 'subsample': 0.7, 'n_estimators': 500000 }

    watchlist = [(d_train, 'train'), (d_test, 'val')]

    xgb_model = xgb.train(xgb_params, d_train, 5000, watchlist, early_stopping_rounds = 50)

    prediction = xgb_model.predict(d_submission)

    final['consumption'] = prediction

    final = final[['time', 'consumption']]

    final.to_csv('submission.csv', index=False)

    print('Successfull save!')



if __name__ == "__main__":
    main()