import pandas as pd
import numpy as np
import warnings

warnings.filterwarnings('ignore')

def readData():

    train = pd.read_csv('train.csv', sep=',', parse_dates=True)

    test = pd.read_csv('test.csv', sep=',', parse_dates=True)

    return (train, test)


def prepareDataBasic(data: pd.DataFrame):

    #This should do simple replacements

    #Reasonable temperature range:

    tempMin: float = -30.
    tempMax: float = 40.
    tempDefault: float = 0.

    #Reasonable dew point range:

    dwptMin: float = 0.
    dwptMax: float = 100.
    dwptDefault: float = 40.

    #Reasonable relative humidity range:

    rhumMin: float = 0.
    rhumMax: float = 120.
    rhumDefault: float = 80.

    #Reasonable one hour precipitation total in mm:

    prcpMin: float = 0.
    prcpMax: float = 50.
    prcpDefault: float = 0.

    #Reasonable snow depth in mm:

    snowMin: float = 0.
    snowMax: float = 50.
    snowDefault: float = 0.

    #Reasonable wind direction range:

    wdirMin: float = 0.
    wdirMax: float = 360.
    wdirDefault: float = 0.

    #Reasonable average wind speed in km/h:

    wspdMin: float = 0.
    wspdMax: float = 80.
    wspdDefault: float = 1.

    #Reasonable peak wind gust in km/h

    wpgtMin: float = 0.
    wpgtMax: float = 80.
    wpgtDefault: float = 0.

    #Reasonable pressure range in hPa

    presMin: float = 800.
    presMax: float = 1298.
    presDefault: float = 960.

    #If temperature, electricity price or consumption is nan, drop row

    data = data.dropna(subset=['temp', 'el_price', 'consumption'])

    # check for ranges => if values are out of range, apply default ones (for now!!!)

    data['temp'] = np.where((np.isnan(data['temp'])) | (data['temp'] > tempMax) | (data['temp'] < tempMin), tempDefault, data['temp'])
    data['dwpt'] = np.where((np.isnan(data['dwpt'])) | (data['dwpt'] > dwptMax) | (data['dwpt'] < dwptMin), dwptDefault, data['dwpt'])
    data['rhum'] = np.where((np.isnan(data['rhum'])) | (data['rhum'] > rhumMax) | (data['rhum'] < rhumMin), rhumDefault, data['rhum'])
    data['prcp'] = np.where((np.isnan(data['prcp'])) | (data['prcp'] > prcpMax) | (data['prcp'] < prcpMin), prcpDefault, data['prcp'])
    data['snow'] = np.where((np.isnan(data['snow'])) | (data['snow'] > snowMax) | (data['snow'] < snowMin), snowDefault, data['snow'])
    data['wdir'] = np.where((np.isnan(data['wdir'])) | (data['wdir'] > wdirMax) | (data['wdir'] < wdirMin), wdirDefault, data['wdir'])
    data['wspd'] = np.where((np.isnan(data['wspd'])) | (data['wspd'] > wspdMax) | (data['wspd'] < wspdMin), wspdDefault, data['wspd'])
    data['wpgt'] = np.where((np.isnan(data['wpgt'])) | (data['wpgt'] > wpgtMax) | (data['wpgt'] < wpgtMin), wpgtDefault, data['wpgt'])
    data['pres'] = np.where((np.isnan(data['pres'])) | (data['pres'] > presMax) | (data['pres'] < presMin), presDefault, data['pres'])


    #Yuck! Dates!

    data['time'] = pd.to_datetime(data.time, utc=True)
    data['time'] = data['time'].dt.tz_convert('Europe/Tallinn')
    data['year'] = data['time'].dt.year
    data['weekday'] = data['time'].dt.dayofweek
    data['dayofyear'] = data['time'].dt.dayofyear
    data['day'] = data['time'].dt.day
    data['month'] = data['time'].dt.month
    data['hour'] = data['time'].dt.hour

    data = data.drop(columns=['time'])

    return data

def prepareDataAdvanced(data: pd.DataFrame):

    #This should look for clever replacements

    return data