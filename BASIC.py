import pandas as pd
import numpy as np
from xgboost import XGBRegressor
import sklearn

df = pd.read_csv('train.csv', sep=',')
dt = pd.read_csv('test.csv', sep=',')

df = df.fillna(0.001)
dt = dt.fillna(0.001)

# dtrain = xgb.DMatrix(df["el_price"], df["consumption"])

# dtest = xgb.DMatrix(dt["el_price"], dt["consumption"])

# param_list = [("objective", "multi:softmax"), ("eval_metric", "merror"), ("num_class", 10)]

# n_rounds = 600

# early_stopping = 50

# eval_list = [(dtrain, 'train'), (dtest, 'test')]

# bst = xgb.train(param_list, dtrain, n_rounds, eval_list, early_stopping_rounds = early_stopping)

# sklearn.metrics.accuracy_score(df['consumption'], bst.predict(xgb.Matrix(dt['el_price'])))

xgb_model = XGBRegressor(objective = 'reg:squarederror', n_jobs=16, learning_rate = 0.01, max_depth=9, n_estimators = 200)

xgb_model.fit(df['el_price'], df['consumption'])

dt['consumption'] = xgb_model.predict(dt['el_price'])

res = dt[['time', 'consumption']].copy()

res.to_csv('submission.csv', index=False)
